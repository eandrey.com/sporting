class AddPositionToGames < ActiveRecord::Migration
  def change
      add_column :games, :latitude, :string
      add_column :games, :longitude, :string
  end
end
