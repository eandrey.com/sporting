class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :sport
      t.string :date
      t.string :place
      t.integer :price_total
      t.string :owner
      t.string :phone
      t.string :duration

      t.timestamps null: false
    end
  end
end
