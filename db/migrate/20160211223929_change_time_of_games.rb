class ChangeTimeOfGames < ActiveRecord::Migration
  def change
      rename_column :games, :time, :time_start
      add_column :games, :time_end, :string
  end
end
