class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :name
      t.integer :number_of_fields
      t.string :street
      t.integer :house
      t.string :notes
      t.string :sports
      t.integer :price_from
      t.integer :price_to
      t.string :phone
      t.string :latitude
      t.string :longitude
      t.text :start_time
      t.text :end_time
      t.text :nickname
      

      t.timestamps null: false
    end
  end
end
