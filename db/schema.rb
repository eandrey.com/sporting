# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160213154731) do

  create_table "games", force: :cascade do |t|
    t.string   "sport"
    t.string   "date"
    t.string   "place"
    t.integer  "price_total"
    t.string   "owner"
    t.string   "phone"
    t.string   "duration"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "time_start"
    t.string   "time_end"
    t.string   "latitude"
    t.string   "longitude"
    t.integer  "number_of_players"
  end

  create_table "places", force: :cascade do |t|
    t.string   "name"
    t.integer  "number_of_fields"
    t.string   "street"
    t.integer  "house"
    t.string   "notes"
    t.string   "sports"
    t.integer  "price_from"
    t.integer  "price_to"
    t.string   "phone"
    t.string   "latitude"
    t.string   "longitude"
    t.text     "start_time"
    t.text     "end_time"
    t.text     "nickname"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

end
