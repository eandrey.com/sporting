class GamesController < ApplicationController
    def index
        @games = Game.all
    end  
    
    def show
        @game = Game.find(params[:id])
    end  
    def new
        @game = Game.new
    end
    def create
        @game = Game.new(game_params) 
        respond_to do |format|
        if @game.save
                format.html { redirect_to games_path, notice: 'Game was successfully created.' }
                format.json { render :show, status: :created, location: @game }
            else
                format.html { render :new }
                format.json { render json: @game.errors, status: :unprocessable_entity }
            end
        end
    end
    
    private
    # Use callbacks to share common setup or constraints between actions.
    def set_place
      @place = Place.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_params
        params["time_start"] = "May"
        params.require(:game).permit(:sport, :date, :time_start, :time_end, :place, :latitude, :longitude,:price_total,:number_of_players)
    end
    
    
end
