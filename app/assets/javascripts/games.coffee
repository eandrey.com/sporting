
$(document).ready ->
    $('#game_date').click ->
        $('.datepickers-container').show();
        
    $('#game_date').datepicker(
            minDate: new Date(),
            dateFormat: 'DD, d M'
            todayButton: true
            autoClose: true
            onSelect: (date, cellType) ->
                console.log "Selected"
        )
   
        
    $('.datepicker--cell').click ->
        $('.datepickers-container').hide();
        
    $('.timepicker').timepicker(
        showMeridian: false
        defaultTime: '18:00'
        explicitMode: true
        showInputs: false
    )