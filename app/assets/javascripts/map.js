var map, iw, drag_area, actual, obj;
var dummy, z_index = 0;

function DummyOView() {
    this.setMap(map);
    this.draw = function() {}
}
DummyOView.prototype = new google.maps.OverlayView();

function fillMarker(a) {
    var m = document.createElement("div");
    m.style.backgroundImage = "url(" + a + ")";
    var b =  "50px";
   	m.style.left = b;
    m.id = obj.id;
    m.className = "drag";
    m.onmousedown = m.ontouchstart = initDrag;
    drag_area.replaceChild(m, obj);
    obj = null
}

function highestOrder() {
    return z_index
}

function createDraggedMarker(d, e) {
    var g = google.maps;
    var f = {
        url: e,
        size: new g.Size(32, 32),
        anchor: new g.Point(15, 32)
    };
    var h = new g.Marker({
        position: d,
        map: map,
        clickable: true,
        draggable: true,
        crossOnDrag: false,
        optimized: false,
        icon: f,
        zIndex: highestOrder()
    });
    g.event.addListener(h, "click", function() {
        actual = h;
        var a = actual.getPosition().lat();
        var b = actual.getPosition().lng();
        var c = "<div class='infowindow'>" + a.toFixed(6) + ", " + b.toFixed(6) + "<\/div>";
        iw.setContent(c);
        iw.open(map, this)
    });
    g.event.addListener(h, "dragstart", function() {
        if (actual == h) iw.close();
        z_index += 1;
        h.setZIndex(highestOrder())
		
    })
	
    g.event.addListener(h, "dragend", function() {
		$('#place_latitude').attr("value", h.getPosition().lat());
		$('#place_longitude').attr("value", h.getPosition().lng());
        $('#game_latitude').attr("value", h.getPosition().lat());
		$('#game_longitude').attr("value", h.getPosition().lng());
    })
}

function initDrag(e) {
    var j = function(e) {
        var a = {};
        if (e && e.touches && e.touches.length) {
            a.x = e.touches[0].clientX;
            a.y = e.touches[0].clientY
        } else {
            if (!e) var e = window.event;
            a.x = e.clientX;
            a.y = e.clientY
        }
        return a
    };
    var k = function(e) {
        if (obj && obj.className == "drag") {
            var i = j(e),
                deltaX = i.x - l.x,
                deltaY = i.y - l.y;
            obj.style.left = (obj.x + deltaX) + "px";
            obj.style.top = (obj.y + deltaY) + "px";
            obj.onmouseup = obj.ontouchend = function() {
                var a = map.getDiv(),
                    mLeft = a.offsetLeft,
                    mTop = a.offsetTop,
                    mWidth = a.offsetWidth,
                    mHeight = a.offsetHeight;
                var b = drag_area.offsetLeft,
                    areaTop = drag_area.offsetTop,
                    oWidth = obj.offsetWidth,
                    oHeight = obj.offsetHeight;
                var x = obj.offsetLeft + b + oWidth / 2;
                var y = obj.offsetTop + areaTop + oHeight / 2;
                if (x > mLeft && x < (mLeft + mWidth) && y > mTop && y < (mTop + mHeight)) {
                    var c = 1;
                    var g = google.maps;
                    var d = new g.Point(x - mLeft - c, y - mTop + (oHeight / 2));
                    var e = dummy.getProjection();
                    var f = e.fromContainerPixelToLatLng(d);
                    var h = obj.style.backgroundImage.slice(4, -1).replace(/"/g, "");
                    createDraggedMarker(f, h);
                    fillMarker(h)
                }
            }
        }
        return false
    };
    if (!e) var e = window.event;
    obj = e.target ? e.target : e.srcElement ? e.srcElement : e.touches ? e.touches[0].target : null;
    if (obj.className != "drag") {
        if (e.cancelable) e.preventDefault();
        obj = null;
        return
    } else {
        z_index += 1;
        obj.style.zIndex = z_index.toString();
        obj.x = obj.offsetLeft;
        obj.y = obj.offsetTop;
        var l = j(e);
        if (e.type === "touchstart") {
            obj.onmousedown = null;
            obj.ontouchmove = k;
            obj.ontouchend = function() {
                obj.ontouchmove = null;
                obj.ontouchend = null;
                obj.ontouchstart = initDrag
            }
        } else {
            document.onmousemove = k;
            document.onmouseup = function() {
                document.onmousemove = null;
                document.onmouseup = null;
                if (obj) obj = null
            }
        }
    }
    return false
}

function buildMap() {
    var g = google.maps;
    var a = {
        center: new g.LatLng(46.4767385, 30.7328248),
        zoom: 14,
        mapTypeId: g.MapTypeId.ROADMAP,
        streetViewControl: false,
        mapTypeControlOptions: {
            mapTypeIds: []
        },
        panControl: false,
        zoomControlOptions: {
            style: g.ZoomControlStyle.SMALL
        }
    };
    map = new g.Map(document.getElementById("map"), a);
    iw = new g.InfoWindow();
    g.event.addListener(map, "click", function() {
        if (iw) iw.close()
    });
    drag_area = document.getElementById("markers");
    var b = drag_area.getElementsByTagName("div");
    for (var i = 0; i < b.length; i++) {
        var c = b[i];
        c.onmousedown = c.ontouchstart = initDrag
    }
    dummy = new DummyOView()
}
window.onload = buildMap;