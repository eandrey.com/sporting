# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


jQuery(document).ready ->
	$('#place_sports').tokenfield ->
	  autocomplete: 
	    source: ['Футбол','Волейбол','Баскетбол','Йога','Бег','Большой теннис','Настольный теннис','Фитнес','Легкая атлетика']
	    delay: 100
	  showAutocompleteOnFocus: true
